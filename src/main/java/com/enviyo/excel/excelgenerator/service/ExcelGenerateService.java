package com.enviyo.excel.excelgenerator.service;

import com.enviyo.excel.excelgenerator.model.Employee;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class ExcelGenerateService {

    private static String[] columns = {"Name", "Email", "Date Of Birth", "Salary"};

    private static List<Employee> employees =  new ArrayList<>();

    // Initializing employees data to insert into the excel file
    static {
        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(1992, 7, 21);
        employees.add(new Employee("Rajeev Singh", "rajeev@example.com",
                dateOfBirth.getTime(), 1200000.0));

        dateOfBirth.set(1965, 10, 15);
        employees.add(new Employee("Thomas cook", "thomas@example.com",
                dateOfBirth.getTime(), 1500000.0));

        dateOfBirth.set(1987, 4, 18);
        employees.add(new Employee("Steve Maiden", "steve@example.com",
                dateOfBirth.getTime(), 1800000.0));
    }

    public void generateExcel() throws IOException {

        //Boş excel dosyasını yaratıyoruz
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
        CreationHelper createHelper = workbook.getCreationHelper();

        // Excel dosyasında bir sheet yaratıyoruz. Excelde altta sayfalar olur ondan ;)
        Sheet sheet = workbook.createSheet("Employee");

        // Excellde başlık olarak kullanılacak hücreler için bir Bold, 14 Punto ve Kırmızı renk bir Font nesnesi yaratıyoruz.
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Excellde başlık olarak kullanılacak hücreler için yukardaki Fontu kullanarak Stil nesnesi yaratıyoruz.
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Excelde tarih alanlarını formatlamak için tarih formatında bir stil yaratıyruz.
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Excell başlıklarını koymak için bir satır atıyoruz.
        int rowIndex = 0;
        Row headerRow = sheet.createRow(rowIndex);

        // Yukarda statik verdiğim başlıklar adedince ilk satıra başlıkları yerleştiriyoruz.
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Çalışan listesini excele satır satır yazıyoruz.
        for(Employee employee: employees) {
            // Bir alt satıra geçmek için indexi bir artırıyoruz.
            rowIndex++;
            Row row = sheet.createRow(rowIndex);

            // 0. sütun adı
            row.createCell(0).setCellValue(employee.getName());

            // 1. sütun email
            row.createCell(1).setCellValue(employee.getEmail());

            // 2. sütun Doğum tarihi
            Cell dateOfBirthCell = row.createCell(2);
            dateOfBirthCell.setCellValue(employee.getDateOfBirth());
            dateOfBirthCell.setCellStyle(dateCellStyle);

            // 3. sütun Maaş
            row.createCell(3).setCellValue(employee.getSalary());
        }

        // Yukardan aşağıya hücrelerin hepsi sığsın diye, auto size ile genişliği ayarlıyoruz.
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        saveWorkbook(workbook);
    }

    private void saveWorkbook(Workbook workbook) throws IOException {
        FileOutputStream fileOut = new FileOutputStream("ExcelTemplate.xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }
}
