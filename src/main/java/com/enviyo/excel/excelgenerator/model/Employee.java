package com.enviyo.excel.excelgenerator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class Employee {
    private String name;
    private String email;
    private Date dateOfBirth;
    private double salary;
}