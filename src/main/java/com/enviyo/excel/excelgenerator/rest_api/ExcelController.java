package com.enviyo.excel.excelgenerator.rest_api;

import com.enviyo.excel.excelgenerator.service.ExcelGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
class ExcelController {

    @Autowired
    private ExcelGenerateService excelGenerateService;

    @GetMapping("/excel")
    public void generateExcel() {

        try{
            excelGenerateService.generateExcel();
        }
        catch(Exception e) {

        }
    }
}
